package br.edu.up;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class GeradorDeTopico {

  public static void main(String[] args) throws NamingException, JMSException {

    // 1. Cria��o do contexto, do t�pico e obten��o do factory;
    Context ctx = new InitialContext();
    Topic topico = (Topic) ctx.lookup("jms/TOPICO_DE_EXEMPLO");
    ConnectionFactory cf = (ConnectionFactory) ctx.lookup("jms/RemoteConnectionFactory");

    // 2. Cria��o da conex�o, da sess�o e do gerador;
    Connection con = cf.createConnection("up", "positivo");
    Session session = con.createSession(false, Session.AUTO_ACKNOWLEDGE);
    MessageProducer gerador = session.createProducer(topico);

    // 3. Cria��o e envio das TextMessages;
    TextMessage msg = session.createTextMessage("Mensagem");
    gerador.send(msg);

    // 4. Encerramento da conex�o e do contexto;
    con.close();
    ctx.close();
  }
}