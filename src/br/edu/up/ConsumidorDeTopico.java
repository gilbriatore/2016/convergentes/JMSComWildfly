package br.edu.up;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class ConsumidorDeTopico {

  public static void main(String[] args) throws NamingException, JMSException {

    // 1. Cria��o do contexto, do t�pico e obten��o do factory;
    Context ctx = new InitialContext();
    Topic topico = (Topic) ctx.lookup("jms/TOPICO_DE_EXEMPLO");
    ConnectionFactory cf = (ConnectionFactory) ctx.lookup("jms/RemoteConnectionFactory");

    // 2. Cria��o e start da conex�o, da sess�o e do consumidor;
    Connection con = cf.createConnection("up", "positivo");
    con.start();
    Session session = con.createSession(false, Session.AUTO_ACKNOWLEDGE);
    MessageConsumer consumidor = session.createConsumer(topico);

    // 3. Leitura das TextMessages;
    System.out.println("Aguardando mensagem...");
    TextMessage msg = (TextMessage) consumidor.receive();
    System.out.println(msg.getText());

    // 4. Encerramento da conex�o e do contexto;
    con.close();
    ctx.close();
  }
}