package br.edu.up;

import java.util.Properties;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;

public class GeradorDeFila {

  public static void main(String[] args) throws Exception {

    // 1. Propriedades de conex�o;
    String url = "http-remoting://127.0.0.1:8080";
    String ic = "org.jboss.naming.remote.client.InitialContextFactory";
    String usuario = "up";
    String senha = "positivo";

    Properties props = new Properties();
    props.put(Context.PROVIDER_URL, url);
    props.put(Context.INITIAL_CONTEXT_FACTORY, ic);
    props.put(Context.SECURITY_PRINCIPAL, usuario);
    props.put(Context.SECURITY_CREDENTIALS, senha);

    // 2. Cria��o do contexto, da fila e obten��o do factory;
    Context ctx = new InitialContext(props);
    Queue queue = (Queue) ctx.lookup("jms/FILA_DE_EXEMPLO");
    ConnectionFactory cf = (ConnectionFactory) ctx.lookup("jms/RemoteConnectionFactory");

    // 3. Cria��o da conex�o, da sess�o e do gerador;
    Connection con = cf.createConnection("up", "positivo");
    Session session = con.createSession(false, Session.AUTO_ACKNOWLEDGE);
    MessageProducer gerador = session.createProducer(queue);

    // 4. Cria��o e envio das TextMessages;
    TextMessage msg = session.createTextMessage("Mensagem");
    gerador.send(msg);

    // 5. Encerramento do contexto;
    ctx.close();
  }
}