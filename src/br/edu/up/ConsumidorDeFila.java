package br.edu.up;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;

public class ConsumidorDeFila {

  public static void main(String[] args) throws Exception {

    // 1. Cria��o do contexto, da fila e obten��o do factory;
    Context ctx = new InitialContext();
    Queue queue = (Queue) ctx.lookup("jms/FILA_DE_EXEMPLO");
    ConnectionFactory cf = (ConnectionFactory) ctx.lookup("jms/RemoteConnectionFactory");

    // 2. Cria��o e start da conex�o e da sess�o;
    Connection connection = cf.createConnection("up", "positivo");
    connection.start();
    Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

    // 3. Cria��o do consumidor e leitura das TextMessages
    MessageConsumer consumidor = session.createConsumer(queue);
    TextMessage msg = (TextMessage) consumidor.receive();
    System.out.println(msg.getText());

    // 4. Encerramento da conex�o e do contexto;
    connection.close();
    ctx.close();
  }
}